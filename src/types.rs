use serenity::client::bridge::gateway::ShardManager;
use std::sync::Arc;
use tokio::sync::Mutex;

pub type ShardHandle = Arc<Mutex<ShardManager>>;
