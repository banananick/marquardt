use serde_json::json;
use serenity::{
    client::Context,
    model::{id::ChannelId, webhook::Webhook},
};

pub async fn create(
    ctx: &Context,
    channel: &ChannelId,
    defaultname: String,
) -> serenity::Result<Webhook> {
    let channel_id = channel;
    let map = json!({ "name": defaultname });
    ctx.http.create_webhook(channel_id.0, &map).await
}
