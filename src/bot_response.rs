use serenity::framework::standard::{
    help_commands::with_embeds, macros::help, Args, CommandGroup, CommandResult, HelpOptions,
};
use serenity::model::channel::Message;
use serenity::model::webhook::Webhook;

use std::collections::HashSet;

use serenity::model::id::UserId;
use serenity::prelude::*;
pub async fn user_too_fast(ctx: &Context, msg: &Message, cooldown_secs: u64) {
    let _ = msg
        .channel_id
        .say(
            &ctx.http,
            &format!(
                "Slow down please, you can try this again in {} seconds.",
                cooldown_secs
            ),
        )
        .await;
}

pub async fn write_as_user(ctx: &Context, message: String, hook: Webhook, uid: UserId) {
    let user = uid.to_user(&ctx.http).await.unwrap();
    let nick = user
        .nick_in(&ctx.http, hook.guild_id.unwrap())
        .await
        .or(Some(user.name.clone()))
        .unwrap();
    let avatar = user.avatar_url().unwrap_or_default();
    match hook
        .execute(&ctx.http, false, |w| {
            w.content(message);
            w.username(nick);
            w.avatar_url(avatar);
            w
        })
        .await
    {
        Ok(_) => {
            log::debug!("Sent webhook message")
        }
        Err(e) => log::error!("Webhook error {:?}", e),
    }
}

#[help]
#[individual_command_tip = "Hello!\n
If you want more information about a specific command, just pass the command as argument."]
#[command_not_found_text = "Could not find command: `{}`."]
#[max_levenshtein_distance(3)]
#[indention_prefix = "*"]
#[lacking_permissions = "Strike"]
#[lacking_role = "Strike"]
#[wrong_channel = "Strike"]
pub async fn help_response(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    let _ = with_embeds(context, msg, args, help_options, groups, owners).await;
    Ok(())
}
