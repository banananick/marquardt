use crate::{
    bot_response::write_as_user,
    typemap::{BotState, ShardContainer},
};
use log::debug;
use serenity::framework::standard::{macros::command, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
#[command]
#[owners_only]
pub async fn quit(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;

    if let Some(manager) = data.get::<ShardContainer>() {
        msg.reply(ctx, "Shutting down!").await?;
        manager.lock().await.shutdown_all().await;
    } else {
        msg.reply(ctx, "There was a problem getting the shard manager")
            .await?;

        return Ok(());
    }

    Ok(())
}

#[command]
#[owners_only]
pub async fn parsetest(ctx: &Context, msg: &Message) -> CommandResult {
    debug!("Parsing {:?}", msg);
    let prefixless = msg.content.replace("!", "");
    if !prefixless.trim().contains(";") {
        log::warn!("Invalid command");
        msg.reply(ctx, "Invalid command arguments").await?;
        return Ok(());
    }
    let params: Vec<&str> = prefixless
        .split_whitespace()
        .nth(1)
        .unwrap()
        .split(";")
        .map(|x| x.trim())
        .collect();
    let target = params[0].parse::<u64>().unwrap_or_default();
    let message: &str = params[1];
    let res = format!("result: {:?}, {:?}, {:?}", params, target, message);
    debug!("{}", res);
    msg.reply(ctx, res).await?;
    Ok(())
}

#[command]
#[owners_only]
pub async fn imposter(ctx: &Context, msg: &Message) -> CommandResult {
    debug!("Parsing {:?}", msg);
    let prefixless = msg.content.replace("!", "");
    if !prefixless.trim().contains(";") {
        log::warn!("Invalid command");
        msg.reply(ctx, "Invalid command arguments").await?;
        return Ok(());
    }
    let params: Vec<&str> = prefixless
        .split_whitespace()
        .nth(1)
        .unwrap()
        .split(";")
        .map(|x| x.trim())
        .collect();
    let target = params[0].parse::<u64>().unwrap_or_default();
    let message: &str = params[1];
    let res = format!("result: {:?}, {:?}, {:?}", params, target, message);
    debug!("{}", res);

    let userid = UserId(target);

    let webhooks = {
        // Since we only want to read the data and not write to it, we open it in read mode,
        // and since this is open in read mode, it means that there can be multiple locks open at
        // the same time, and as mentioned earlier, it's heavily recommended that you only open
        // the data lock in read mode, as it will avoid a lot of possible deadlocks.
        let data_read = ctx.data.read().await;

        // Then we obtain the value we need from data, in this case, we want the command counter.
        // The returned value from get() is an Arc, so the reference will be cloned, rather than
        // the data.
        data_read
            .get::<BotState>()
            .expect("Expected BotState in TypeMap.")
            .clone()
    };
    for hook in webhooks {
        write_as_user(&ctx, message.to_owned(), hook, userid).await;
    }
    Ok(())
}
