use super::commands::{meta::*, owner::*};
use serenity::framework::standard::macros::group;

#[group]
#[commands(ping, poke)]
pub struct Meta;

#[group]
#[commands(quit, imposter, parsetest)]
pub struct Owner;
