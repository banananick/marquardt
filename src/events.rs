use crate::config::{EnvKey, CONFIG};
use log::*;
use serenity::{
    async_trait,
    framework::standard::{macros::hook, CommandResult, DispatchError},
    model::{channel::Message, event::ResumedEvent, gateway, webhook::Webhook},
};
use serenity::{model::id::ChannelId, prelude::*};

use crate::{
    bot_response::user_too_fast,
    hookutils,
    typemap::{BotOwner, BotState},
};

use std::{collections::HashSet, sync::atomic::AtomicBool, u64};
pub struct BotEventHandler {
    _running: AtomicBool,
}

impl Default for BotEventHandler {
    fn default() -> Self {
        BotEventHandler {
            _running: AtomicBool::new(false),
        }
    }
}

#[async_trait]
impl EventHandler for BotEventHandler {
    async fn message(&self, _: Context, msg: Message) {
        let channel = msg.channel_id.as_u64();

        trace!("Received message in {} \n\t{}'", channel, msg.content);
    }

    async fn ready(&self, ctx: Context, ready: gateway::Ready) {
        // Here we setup all tokio tasks that will be needed as well

        // Here we do context specific setup

        let hellomsg = format!("{} is connected!", ready.user.name);
        info!("{}", &hellomsg);
        let typemap = ctx.data.read().await;
        let owners = typemap
            .get::<BotOwner>()
            .expect("Expected BotOwner in TypeMap.");

        for owner in owners {
            if let Ok(user) = owner.to_user(&ctx).await {
                let dm = user.direct_message(&ctx, |m| m.content(&hellomsg)).await;

                if let Err(why) = dm {
                    warn!("Failed sending DM message to owner {:?}: {}", owner, why);
                }
            } else {
                warn!("Sending DM to owner {:?} failed", owner);
            }
        }
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        warn!("Bot was disconnected but connected again");
    }

    async fn cache_ready(&self, ctx: Context, guildids: Vec<serenity::model::id::GuildId>) {
        debug!("Guilds in cache: {:?}", guildids);
        let hooklist = CONFIG
            .get(&EnvKey::Hooks)
            .unwrap()
            .split(',')
            .map(|x| x.trim().parse::<u64>().unwrap_or_default())
            .collect::<Vec<_>>();

        let mut newhooks: Vec<Webhook> = Vec::default();
        for id in guildids {
            let cached_channels = id.channels(&ctx.http).await.unwrap_or_default();

            for chan in &hooklist {
                debug!("check {:?}", chan);
                if !&cached_channels
                    .iter()
                    .map(|y| *(y.0).as_u64())
                    .any(|x| x == *chan)
                {
                    debug!("Already hooked: {:?}", chan);
                    continue;
                }
                let chan = ChannelId(*chan);
                let active_webhooks = chan.webhooks(&ctx.http).await.unwrap_or_default();

                let unique_active_hooks = active_webhooks
                    .iter()
                    .map(|y| y.id.as_u64().clone())
                    .collect::<HashSet<_>>();
                let unique_new_hooks = hooklist.iter().map(|z| z.clone()).collect::<HashSet<_>>();
                if unique_active_hooks.intersection(&unique_new_hooks).count() > 0 {
                    debug!("Hook already registered");
                    continue;
                }

                //  let diff = unique_active_hooks.difference(&unique_new_hooks);
                //debug!("Difference count: {:?}", diff);
                let diff = unique_new_hooks.clone();
                for _iid in diff {
                    let newchan = ChannelId(_iid);
                    debug!(
                        "Adding hooks to channel: {} ({:?})",
                        newchan
                            .name(&ctx.cache)
                            .await
                            .unwrap_or("Unknown Channel".to_owned()),
                        &newchan
                    );

                    match hookutils::create(&ctx, &newchan, "Sven 2".to_owned()).await {
                        Ok(hook) => {
                            newhooks.push(hook);
                        }
                        Err(e) => {
                            error!("\tFailed to create hook: {:?}", e);
                        }
                    }
                }
            }
        }

        let mut state_promise = ctx.data.as_ref().write().await;

        state_promise
            .get_mut::<BotState>()
            .expect("Expected BotState in TypeMap.")
            .append(&mut newhooks);
    }

    async fn channel_create(
        &self,
        _ctx: Context,
        _channel: &serenity::model::channel::GuildChannel,
    ) {
        debug!("channel_create: {:?}", _channel);
    }

    async fn category_create(
        &self,
        _ctx: Context,
        _category: &serenity::model::channel::ChannelCategory,
    ) {
    }

    async fn category_delete(
        &self,
        _ctx: Context,
        _category: &serenity::model::channel::ChannelCategory,
    ) {
    }

    async fn channel_delete(
        &self,
        _ctx: Context,
        _channel: &serenity::model::channel::GuildChannel,
    ) {
    }

    async fn channel_pins_update(
        &self,
        _ctx: Context,
        _pin: serenity::model::event::ChannelPinsUpdateEvent,
    ) {
    }

    async fn channel_update(
        &self,
        _ctx: Context,
        _old: Option<serenity::model::channel::Channel>,
        _new: serenity::model::channel::Channel,
    ) {
        debug!("channel_update: {:?} / {:?}", _old, _new);
    }

    async fn guild_ban_addition(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _banned_user: serenity::model::prelude::User,
    ) {
    }

    async fn guild_ban_removal(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _unbanned_user: serenity::model::prelude::User,
    ) {
    }

    async fn guild_create(
        &self,
        _ctx: Context,
        _guild: serenity::model::guild::Guild,
        _is_new: bool,
    ) {
    }

    async fn guild_delete(
        &self,
        _ctx: Context,
        _incomplete: serenity::model::guild::GuildUnavailable,
        _full: Option<serenity::model::guild::Guild>,
    ) {
    }

    async fn guild_emojis_update(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _current_state: std::collections::HashMap<
            serenity::model::id::EmojiId,
            serenity::model::guild::Emoji,
        >,
    ) {
    }

    async fn guild_integrations_update(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
    ) {
    }

    async fn guild_member_addition(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _new_member: serenity::model::guild::Member,
    ) {
        debug!(
            "guild_member_addition: {:?} => {:?}",
            _guild_id, _new_member
        );
    }

    async fn guild_member_removal(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _user: serenity::model::prelude::User,
        _member_data_if_available: Option<serenity::model::guild::Member>,
    ) {
    }

    async fn guild_member_update(
        &self,
        _ctx: Context,
        _old_if_available: Option<serenity::model::guild::Member>,
        _new: serenity::model::guild::Member,
    ) {
        debug!("guild_member_update: {:?} => {:?}", _old_if_available, _new);
    }

    async fn guild_members_chunk(
        &self,
        _ctx: Context,
        _chunk: serenity::model::event::GuildMembersChunkEvent,
    ) {
    }

    async fn guild_role_create(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _new: serenity::model::guild::Role,
    ) {
    }

    async fn guild_role_delete(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _removed_role_id: serenity::model::id::RoleId,
        _removed_role_data_if_available: Option<serenity::model::guild::Role>,
    ) {
    }

    async fn guild_role_update(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _old_data_if_available: Option<serenity::model::guild::Role>,
        _new: serenity::model::guild::Role,
    ) {
    }

    async fn guild_unavailable(&self, _ctx: Context, _guild_id: serenity::model::id::GuildId) {
        debug!("guild_unavailable: {:?}", _guild_id);
    }

    async fn guild_update(
        &self,
        _ctx: Context,
        _old_data_if_available: Option<serenity::model::guild::Guild>,
        _new_but_incomplete: serenity::model::guild::PartialGuild,
    ) {
        debug!(
            "guild_update: {:?} | {:?}",
            _old_data_if_available, _new_but_incomplete
        );
    }

    async fn invite_create(&self, _ctx: Context, _data: serenity::model::event::InviteCreateEvent) {
    }

    async fn invite_delete(&self, _ctx: Context, _data: serenity::model::event::InviteDeleteEvent) {
    }

    async fn message_delete(
        &self,
        _ctx: Context,
        _channel_id: serenity::model::id::ChannelId,
        _deleted_message_id: serenity::model::id::MessageId,
        _guild_id: Option<serenity::model::id::GuildId>,
    ) {
    }

    async fn message_delete_bulk(
        &self,
        _ctx: Context,
        _channel_id: serenity::model::id::ChannelId,
        _multiple_deleted_messages_ids: Vec<serenity::model::id::MessageId>,
        _guild_id: Option<serenity::model::id::GuildId>,
    ) {
    }

    async fn message_update(
        &self,
        _ctx: Context,
        _old_if_available: Option<Message>,
        _new: Option<Message>,
        _event: serenity::model::event::MessageUpdateEvent,
    ) {
    }

    async fn reaction_add(&self, _ctx: Context, _add_reaction: serenity::model::channel::Reaction) {
    }

    async fn reaction_remove(
        &self,
        _ctx: Context,
        _removed_reaction: serenity::model::channel::Reaction,
    ) {
    }

    async fn reaction_remove_all(
        &self,
        _ctx: Context,
        _channel_id: serenity::model::id::ChannelId,
        _removed_from_message_id: serenity::model::id::MessageId,
    ) {
    }

    async fn presence_replace(
        &self,
        _ctx: Context,
        precences: Vec<serenity::model::prelude::Presence>,
    ) {
        debug!("presence_replace: {:?}", precences);
    }

    async fn presence_update(
        &self,
        _ctx: Context,
        _new_data: serenity::model::event::PresenceUpdateEvent,
    ) {
        debug!("presence_update: {:?}", _new_data);
    }

    async fn shard_stage_update(
        &self,
        _ctx: Context,
        shardupdate: serenity::client::bridge::gateway::event::ShardStageUpdateEvent,
    ) {
        debug!("shard_stage_update: {:?}", shardupdate);
    }

    async fn typing_start(&self, _ctx: Context, _: serenity::model::event::TypingStartEvent) {}

    async fn unknown(&self, _ctx: Context, _name: String, _raw: serde_json::Value) {
        warn!("Unknown event: {:?}\n{:#?}", _name, _raw);
    }

    async fn user_update(
        &self,
        _ctx: Context,
        _old_data: serenity::model::prelude::CurrentUser,
        _new: serenity::model::prelude::CurrentUser,
    ) {
        debug!("Guilds in cache: Old: {:?} / New: {:?}", _old_data, _new);
    }

    async fn voice_server_update(
        &self,
        _ctx: Context,
        _: serenity::model::event::VoiceServerUpdateEvent,
    ) {
    }

    async fn voice_state_update(
        &self,
        _ctx: Context,
        _: Option<serenity::model::id::GuildId>,
        _old: Option<serenity::model::prelude::VoiceState>,
        _new: serenity::model::prelude::VoiceState,
    ) {
    }

    async fn webhook_update(
        &self,
        _ctx: Context,
        _guild_id: serenity::model::id::GuildId,
        _belongs_to_channel_id: serenity::model::id::ChannelId,
    ) {
        debug!(
            "webhokupdate @ {:?} in {:?}",
            _guild_id, _belongs_to_channel_id
        );
    }
}

#[hook]
pub async fn dispatch_error(ctx: &Context, msg: &Message, error: DispatchError) {
    match error {
        DispatchError::CheckFailed(check, ref reason) => {
            error!("{:?}: ({}) {}", &error, check, reason);
        }
        DispatchError::CommandDisabled(ref cmd) => {
            error!("{:?}: {}", &error, &cmd);
        }
        DispatchError::BlockedUser => {
            error!("{:?}", &error);
        }
        DispatchError::BlockedGuild => {
            error!("{:?}", &error);
        }
        DispatchError::BlockedChannel => {
            error!("{:?}", &error);
        }
        DispatchError::OnlyForDM => {
            error!("{:?}", &error);
        }
        DispatchError::OnlyForGuilds => {
            error!("{:?}", &error);
        }
        DispatchError::OnlyForOwners => {
            error!("{:?}", &error);
        }
        DispatchError::LackingRole => {
            error!("{:?}", &error);
        }
        DispatchError::LackingPermissions(perms) => {
            error!("{:?} ... my permissions are {:#?}", &error, perms);
        }
        DispatchError::NotEnoughArguments { min, given } => {
            error!("{:?} ... min is {} but {} was given", &error, min, given)
        }
        DispatchError::TooManyArguments { max, given } => {
            error!("{:?} ... max is {} but {} was given", &error, max, given)
        }
        DispatchError::Ratelimited(info) => {
            if info.is_first_try {
                user_too_fast(ctx, msg, info.as_secs()).await;
            }
        }
        _ => {
            error!("Unknown dispatch error")
        }
    };
}

#[hook]
pub async fn unknown_command(ctx: &Context, msg: &Message, unknown_command_name: &str) {
    let answer = format!("Could not find command '{}'", unknown_command_name);
    let ext_answer = format!("{}\nTry !help to see a list of all commands", answer);
    warn!("{}", answer);

    let dm = msg
        .author
        .direct_message(&ctx, |m| m.content(&ext_answer))
        .await;

    if let Err(why) = dm {
        error!("Failed sending DM message: {:?}", why);

        let _ = msg
            .reply(&ctx, "- Unknown command!\n- Failed to send you a DM.")
            .await;
    }
}

#[hook]
pub async fn after_command(
    _ctx: &Context,
    _msg: &Message,
    command_name: &str,
    command_result: CommandResult,
) {
    match command_result {
        Ok(()) => trace!("Processed command '{}'", command_name),
        Err(why) => error!("Command '{}' returned error {:?}", command_name, why),
    }
}

#[hook]
pub async fn before_command(_ctx: &Context, msg: &Message, command_name: &str) -> bool {
    debug!(
        "Received command '{}' by user '{}'",
        command_name, msg.author.name
    );

    true // if `before` returns false, command processing doesn't happen.
}
