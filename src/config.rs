use dotenv::dotenv;
use lazy_static::lazy_static;
use std::string::String;
use std::{collections::HashMap, env};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum EnvKey {
    BotToken,
    Hooks,
}

fn safe_env(name: &str) -> String {
    match env::var(name) {
        Ok(v) => v,
        Err(e) => {
            log::error!("Missing environment variable: {:?} ({:?})", name, e);
            String::default()
        }
    }
}

lazy_static! {
    pub static ref CONFIG: HashMap<EnvKey, String> = {
        log::info!("Loading environment...");
        let env_path = match dotenv() {
            Ok(p) => p,
            Err(e) => panic!("No dotenv found: {}", &e),
        };
        log::info!("Loading env from {}", env_path.display());
        let mut m = HashMap::new();
        m.insert(EnvKey::BotToken, safe_env("DISCORD_TOKEN"))
            .or(None);
        m.insert(EnvKey::Hooks, safe_env("HOOK_INTO")).or(None);
        m
    };
}
