use std::collections::HashSet;

use log::{debug, info};
use serenity::{client::bridge::gateway::GatewayIntents, http::Http, model::id::UserId};

pub async fn load_appinfo(bot_token: &str) -> (HashSet<UserId>, UserId) {
    info!("Fetching owner and bot id numbers");
    let request = Http::new_with_token(bot_token);
    match request.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            if let Some(team) = info.team {
                owners.insert(team.owner_user_id);
            } else {
                owners.insert(info.owner.id);
            }
            debug!("My owners are {:#?}", owners);
            match request.get_current_user().await {
                Ok(bot) => (owners, bot.id),
                Err(why) => panic!("Failed to load bot info: {:?}", why),
            }
        }
        Err(why) => panic!("Could not access application info: {:?}", why),
    }
}
pub fn intents() -> GatewayIntents {
    return GatewayIntents::DIRECT_MESSAGE_REACTIONS
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::DIRECT_MESSAGE_TYPING
        | GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::GUILD_MESSAGE_TYPING
        | GatewayIntents::GUILD_MESSAGE_REACTIONS
        | GatewayIntents::GUILD_WEBHOOKS
        | GatewayIntents::GUILD_INTEGRATIONS
        | GatewayIntents::GUILDS;
}
