use serenity::model::{id::UserId, webhook::Webhook};
use serenity::prelude::*;
use std::collections::HashSet;

use super::types::ShardHandle;

pub struct BotState;
pub struct BotOwner;
pub struct ShardContainer;

impl TypeMapKey for BotState {
    type Value = Vec<Webhook>;
}

impl TypeMapKey for BotOwner {
    type Value = HashSet<UserId>;
}

impl TypeMapKey for ShardContainer {
    type Value = ShardHandle;
}
