pub mod bot_response;
pub mod commands;
pub mod config;
pub mod discord;
pub mod events;
pub mod groups;
mod hookutils;
pub mod typemap;
mod types;

use config::{EnvKey, CONFIG};
use log::*;
use serenity::framework::standard::StandardFramework;
use std::{env, sync::Arc};
use stderrlog;
use structopt::StructOpt;

use bot_response::*;
use groups::*;
use serenity::prelude::*;
use typemap::*;

fn match_loglevel(user_str: &str) -> usize {
    let level_str = user_str.to_owned().trim().to_lowercase();
    return match level_str.as_ref() {
        "error" => 0,
        "warn" => 1,
        "info" => 2,
        "debug" => 3,
        "trace" => 4,
        _ => 5,
    };
}

/// A basic example
#[derive(StructOpt, Debug)]
#[structopt()]
struct AppArguments {
    #[structopt(short = "l", long = "log", parse(from_str = match_loglevel))]
    log_level: usize,
}

//

async fn configure() -> AppArguments {
    let args = AppArguments::from_args();

    stderrlog::new()
        .module(module_path!())
        .quiet(false)
        .verbosity(args.log_level)
        .timestamp(stderrlog::Timestamp::Second)
        .init()
        .unwrap();

    args
}

#[tokio::main]
async fn main() {
    log::info!("{} - {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));

    let _args = configure().await;
    let token = CONFIG.get(&EnvKey::BotToken).unwrap();
    let (owners, bot_id) = discord::load_appinfo(token).await;

    let framework = StandardFramework::new()
        .configure(|cfg| {
            cfg.with_whitespace(true)
                .on_mention(Some(bot_id))
                .prefix("!")
                .delimiters(vec![" ,", ","])
                .owners(owners.clone())
                .allow_dm(true)
                .case_insensitivity(true)
        })
        .before(events::before_command)
        .after(events::after_command)
        .unrecognised_command(events::unknown_command)
        .on_dispatch_error(events::dispatch_error)
        .help(&HELP_RESPONSE)
        .group(&META_GROUP)
        .group(&OWNER_GROUP);

    info!("Begin login");

    let mut client = Client::builder(&token)
        .event_handler(events::BotEventHandler::default())
        .intents(discord::intents())
        .framework(framework)
        .await
        .expect("Failed client login");

    {
        info!("Loading state...");
        let mut data = client.data.write().await;
        data.insert::<BotState>(Vec::default());
        data.insert::<BotOwner>(owners.clone());
        data.insert::<ShardContainer>(Arc::clone(&client.shard_manager));
    }

    let shard_manager = client.shard_manager.clone();

    debug!("Setup SIGINT handler...");
    tokio::spawn(async move {
        tokio::signal::ctrl_c()
            .await
            .expect("Could not register ctrl+c handler");
        shard_manager.lock().await.shutdown_all().await;
        info!("Goodbye!")
    });

    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }
}
